import { PostComponent } from './post/post.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

@NgModule({ //Чтобы превратить компонент в класс - нужно добавить к нему метаданные. Декоратор - функция которая это делает
  declarations: [
    AppComponent,
    PostComponent //После экспорта класса компонента его необходимо обязательно импортировать в массив declaration
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
