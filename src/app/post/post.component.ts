import { Component } from "@angular/core"; //Строка прописывается автоматически при написании @Component

@Component({
  selector: 'app-post',
  templateUrl: 'post.component.html',
  styleUrls: ['post.component.scss'] //Поле стилей передается как массив, т.к. в нем может быть не один файл стилей
})

//По гайдлайну имя компонента с большой буквы
export class PostComponent { //Обязательно нужно экспортировать класс, чтобы другие компоненты могли с ним взаимодействовать

}
